#pragma once

#include <algorithm>
#include <any>
#include <cctype>
#include <map>
#include <optional>
#include <memory>
#include <string>
#include <vector>

#include <fmt/core.h>


namespace cli2b {

using optional_string = std::optional<std::string>;
using any_vector = std::vector<std::any>;

enum EntryType {
    UNDEFINED,
    INT,
    FLOAT,
    STRING,
    FLAG,
};
static char const *EntryTypeNames[] = {
    "UNDEFINED",
    "INT",
    "FLOAT",
    "STRING",
    "FLAG",
};
struct EntryDefinition {
private:
    optional_string _short_name, _long_name, _help_msg;
    EntryType _option_type;
public:
    EntryDefinition(): _short_name({}), _long_name({}), _help_msg({}), _option_type(UNDEFINED) {};
    ~EntryDefinition() = default;

    // indicators
    bool has_short_name() const { return this->_short_name.has_value(); }
    bool has_long_name() const { return this->_long_name.has_value(); }
    bool has_help_msg() const { return this->_help_msg.has_value(); }
    bool has_option_type() const { return this->_option_type != EntryType::UNDEFINED; }

    // initializers
    EntryDefinition short_name(std::string const &short_name_) { this->_short_name = short_name_; return *this; }
    EntryDefinition long_name(std::string const &long_name_) { this->_long_name = long_name_; return *this; }
    EntryDefinition help_msg(std::string const &help_msg_) { this->_help_msg = help_msg_; return *this; }
    EntryDefinition option_type(EntryType const &option_type) { this->_option_type = option_type; return *this; }

    // get
    optional_string get_short_name() const { return this->_short_name; }
    optional_string get_long_name() const { return this->_long_name; }
    optional_string get_help_msg() const { return this->_help_msg; }
    EntryType get_option_type() const { return this->_option_type; }
    char const *get_option_type_name() const { return EntryTypeNames[this->_option_type]; }

    // checker
    bool is_valid() const {
        return (this->has_short_name() || this->has_long_name()) && this->has_option_type();
    }
};

class bad_option_error: public std::runtime_error {
public:
    bad_option_error(std::string const &option_name, std::string adjective):
        std::runtime_error(fmt::format("bad {} option from command line: '{}'", adjective, option_name)) {}
};
class bad_subcommand_error: public std::runtime_error {
public:
    bad_subcommand_error(std::string const &subcommand_name):
        std::runtime_error(fmt::format("bad subcommand from command line: '{}'", subcommand_name)) {}
};
class unregistered_option_error: public std::runtime_error {
public:
    unregistered_option_error(std::string const &option_name, std::string adjective):
        std::runtime_error(fmt::format("unregistered {} option: '{}'", adjective, option_name)) {}
};
class unregistered_subcommand_error: public std::runtime_error {
public:
    unregistered_subcommand_error(std::string const &subcommand_name):
        std::runtime_error(fmt::format("unregistered subcommand: '{}'", subcommand_name)) {}
};
class missing_argument_error: public std::runtime_error {
public:
    missing_argument_error(std::string const &option_name):
        std::runtime_error(fmt::format("missing argument for option: '{}'", option_name)) {}
};
class flag_with_argument_error: public std::runtime_error {
public:
    flag_with_argument_error(std::string const &option_name, std::string const &option_argstr):
        std::runtime_error(fmt::format("received argument '{}' for flag: '{}'", option_argstr, option_name)) {}
};
class unspecified_option_error: public std::runtime_error {
public:
    unspecified_option_error(std::string const &option_name):
        std::runtime_error(fmt::format("unspecified option: '{}'", option_name)) {}
};
class option_type_mismatch_error: public std::logic_error {
public:
    option_type_mismatch_error(std::string const &option_name, EntryType option_type):
        std::logic_error(fmt::format("type mismatch for option '{}' which is of definition type '{}'", option_name, EntryTypeNames[option_type])) {}
};

class CLI {
private:
    // @brief Name of this CLI
    std::string name, description;
    // @brief Pointer to the command that contains current command as a subcommand, value is null
    // current command is not a subcommand.
    std::optional<std::shared_ptr<CLI const>> supercommand;
    // @brief The raw definitions of CLI options
    std::vector<std::shared_ptr<EntryDefinition>> option_definitions;
    // @brief Map from option names to their raw option definitions, for short and long options
    // respectively
    std::map<std::string, std::shared_ptr<EntryDefinition>> registry_short, registry_long;
    // @brief Map from option names to corresponding array of values, for short and long options
    std::map<std::string, std::shared_ptr<any_vector>> values_short, values_long;
    // @brief Next index of argv to be parsed
    std::size_t argv_offset;
    // @brief Subcommands
    std::map<std::string, std::shared_ptr<CLI>> subcommands;

private:
    // @brief Store the raw option definitions internally and refresh the option name to option
    // definition mapping according to the raw option definitions
    void init_definitions(std::vector<EntryDefinition> const &);
    // @brief Store the subcommands
    void init_subcommands(std::vector<CLI> const &);
    // @brief Parse a command line option
    // @return Tuple of [is_long_option, option_name, option_argstr]
    std::tuple<std::optional<bool>, optional_string, optional_string> parse_option(std::string const &) const;

public:
    CLI() = delete;
    virtual ~CLI() = default;

    // move
    CLI(CLI &&) = default;
    CLI &operator=(CLI &&) = default;

    // copy
    CLI(CLI const &) = default;

    // @brief Init with options/flags and subcommands
    CLI(
        std::string const &name_,
        std::string const &description_,
        std::vector<EntryDefinition> const &,
        std::vector<CLI> const &
    );

public:
    // @return Command name defined during initialization
    std::string const get_command_name() const;
    // @return A help message according to the raw option definitions
    std::string const get_help_msg() const;
    // @return Mapping of subcommand_name -> shared_ptr<subcommand>
    std::map<std::string, std::shared_ptr<CLI>> const get_subcommands() const;
    // @return The subcommand with name specified by parameter
    CLI const &get_subcommand(std::string const &) const;
    // @return Count of parsed argv
    std::size_t get_parsed_argv_cnt() const;
    // @brief Parse command line args and store them
    // @return Number of successfully read values
    std::size_t parse(int, char const **);
    // @return Number of occurrence(s) a long option/flag has been specified via command line
    std::size_t occurrence_long(std::string const &) const;
    // @return Number of occurrence(s) a short option/flag has been specified via command line
    std::size_t occurrence_short(std::string const &) const;
    // @return Whether a subcommand has been supplied
    bool occurrence_subcommand(std::string const &) const;
    // @brief Get the array of option values for a short option
    template<typename value_type> std::vector<value_type> get_short(std::string const &option_name) const {
        if (!this->values_short.contains(option_name)) {
            throw unspecified_option_error(option_name);
        }
        std::shared_ptr<any_vector> val_ptr = this->values_short.at(option_name);
        std::vector<value_type> ret(val_ptr->size());
        try {
            std::transform(val_ptr->begin(), val_ptr->end(), ret.begin(),
                [&](std::any const &val) { return std::any_cast<value_type>(val); });
        } catch(std::bad_any_cast const &) {
            throw option_type_mismatch_error(option_name, this->registry_short.at(option_name)->get_option_type());
        }
        return ret;
    }
    // @brief Get the array of option values for a long option
    template<typename value_type> std::vector<value_type> get_long(std::string const &option_name) const {
        if (!this->values_long.contains(option_name)) {
            throw unspecified_option_error(option_name);
        }
        std::shared_ptr<any_vector> val_ptr = this->values_long.at(option_name);
        std::vector<value_type> ret(val_ptr->size());
        try {
            std::transform(val_ptr->begin(), val_ptr->end(), ret.begin(),
                [&](std::any const &val) { return std::any_cast<value_type>(val); });
        } catch(std::bad_any_cast const &) {
            throw option_type_mismatch_error(option_name, this->registry_long.at(option_name)->get_option_type());
        }
        return ret;
    }
};

}
