#include <iostream>
#include <cassert>

#include "cli2b.hpp"

namespace cli2b {

CLI::CLI(
    std::string const &name_,
    std::string const &description_,
    std::vector<EntryDefinition> const &option_definitions,
    std::vector<CLI> const &subcommands
): name(name_), description(description_), argv_offset(0) {
    this->init_definitions(option_definitions);
    this->init_subcommands(subcommands);
}

void CLI::init_definitions(std::vector<EntryDefinition> const &option_definitions) {
    // ensure there exists no partially declared option
    assert(std::all_of(option_definitions.begin(), option_definitions.end(),
                       [&](EntryDefinition const &opt_def) {
                           return opt_def.is_valid();
                        }));
    this->option_definitions = std::vector<std::shared_ptr<EntryDefinition>>(option_definitions.size());
    std::transform(option_definitions.begin(), option_definitions.end(), this->option_definitions.begin(),
                   [&](EntryDefinition const &opt_def) { return std::make_shared<EntryDefinition>(opt_def); });
    // register internally
    this->registry_short = decltype(this->registry_short){};
    this->registry_long = decltype(this->registry_long){};
    for (auto const &option_def: this->option_definitions) {
        std::shared_ptr<any_vector> initial_args = std::make_shared<any_vector>();
        if (option_def->has_short_name()) {
            std::string short_name = option_def->get_short_name().value();
            assert(!this->registry_short.contains(short_name));
            this->registry_short.insert(std::make_pair(short_name, option_def));
            this->values_short[short_name] = initial_args;
        }
        if (option_def->has_long_name()) {
            std::string long_name = option_def->get_long_name().value();
            assert(!this->registry_long.contains(long_name));
            this->registry_long.insert(std::make_pair(long_name, option_def));
            this->values_long[long_name] = initial_args;
        }
    }
}

void CLI::init_subcommands(std::vector<CLI> const &subcommands) {
    std::shared_ptr<CLI> shared_this = std::make_shared<CLI>(*this);
    this->subcommands = decltype(this->subcommands){};
    for (auto const &subc: subcommands) {
        std::string subc_name = subc.get_command_name();
        assert(!this->subcommands.contains(subc_name));
        CLI copied_subc{subc};
        copied_subc.supercommand = shared_this;
        this->subcommands.insert(std::make_pair(copied_subc.get_command_name(), std::make_shared<CLI>(copied_subc)));
    }
}

std::tuple<std::optional<bool>, optional_string, optional_string> CLI::parse_option(std::string const &opt) const {
    if (!opt.starts_with("-")) {
        return {{}, {}, {}};
    }
    bool is_long_option = opt.starts_with("--");
    auto equal_pos = opt.find("=");
    int option_name_begin_pos = (int)is_long_option + 1;
    auto option_name_end_pos = equal_pos == std::string::npos ? opt.length() : equal_pos;
    optional_string option_name = opt.substr(option_name_begin_pos, option_name_end_pos - option_name_begin_pos);
    optional_string option_argstr = equal_pos == std::string::npos ? optional_string{} : opt.substr(equal_pos + 1);
    return {is_long_option, option_name, option_argstr};
}

std::size_t CLI::parse(int argc, char const **argv) {
    std::size_t read_options = 0;
    for (this->argv_offset = 1; this->argv_offset < argc; ++this->argv_offset) {
        std::string opt = argv[this->argv_offset];
        std::shared_ptr<EntryDefinition> opt_def;

        auto [is_long_option, option_name, option_argstr] = this->parse_option(opt);

        if (!is_long_option.has_value()) {
            if (this->subcommands.contains(opt) && !this->supercommand.has_value()) {
                return read_options + this->subcommands[opt]->parse(argc - this->argv_offset, argv + this->argv_offset);
            } else {
                throw bad_subcommand_error(opt);
            }
        } else if (is_long_option.value()) {
            if (option_name.value().length() == 0) {  // special argument `--` per the GNU standards
                return read_options;
            }
            if (!this->registry_long.contains(option_name.value())) {
                throw bad_option_error(option_name.value(), "long");
            }
            opt_def = this->registry_long.at(option_name.value());
        } else {
            if (!this->registry_short.contains(option_name.value())) {
                throw bad_option_error(option_name.value(), "short");
            }
            opt_def = this->registry_short.at(option_name.value());
        }

        if (opt_def->get_option_type() == EntryType::FLAG) {
            if (option_argstr.has_value()) {
                throw flag_with_argument_error(option_name.value(), option_argstr.value());
            }
        } else if (!option_argstr.has_value()) {
            ++this->argv_offset;
            if (this->argv_offset == argc) {
                throw missing_argument_error(opt);
            }
            option_argstr = argv[this->argv_offset];
        } else { /* noop */ }

        std::any option_value;
        switch (opt_def->get_option_type()) {
            case EntryType::UNDEFINED:
                throw std::runtime_error(fmt::format("type for option '{}' is uninitialized", option_name.value()));
            case EntryType::INT:
                try {
                    option_value = std::stoi(option_argstr.value().c_str());
                } catch (std::invalid_argument const &) {
                    throw option_type_mismatch_error(option_name.value(), EntryType::INT);
                }
                break;
            case EntryType::FLOAT:
                option_value = std::atof(option_argstr.value().c_str());
                break;
            case EntryType::STRING:
                option_value = std::string{option_argstr.value()};
                break;
            case EntryType::FLAG:
                option_value = {};
                break;
            default:
                throw std::runtime_error(fmt::format("unsupported option type: {}", opt_def->get_option_type_name()));
        }

        if (is_long_option.value()) {
            this->values_long[option_name.value()]->push_back(option_value);
        } else {
            this->values_short[option_name.value()]->push_back(option_value);
        }
        ++read_options;
    }
    return read_options;
}

std::size_t CLI::occurrence_long(std::string const &option_name) const {
    return this->values_long.contains(option_name) ? this->values_long.at(option_name)->size() : 0;
}
std::size_t CLI::occurrence_short(std::string const &option_name) const {
    return this->values_short.contains(option_name) ? this->values_short.at(option_name)->size() : 0;
}
bool CLI::occurrence_subcommand(std::string const &subcommand_name) const {
    return this->subcommands.contains(subcommand_name)
        ? (this->subcommands.at(subcommand_name)->get_parsed_argv_cnt() > 0)
        : false;
}

std::string const CLI::get_command_name() const {
    return this->name;
}
std::map<std::string, std::shared_ptr<CLI>> const CLI::get_subcommands() const {
    return this->subcommands;
}
CLI const &CLI::get_subcommand(std::string const &name) const {
    if (!this->subcommands.contains(name)) {
        throw unregistered_subcommand_error(name);
    } else {
        return *this->subcommands.at(name);
    }
}
std::size_t CLI::get_parsed_argv_cnt() const {
    return this->argv_offset;
}

std::string const CLI::get_help_msg() const {
    std::string usage_msg = this->supercommand.has_value()
        ? fmt::format("USAGE:\n    {} {}", this->supercommand.value()->name, this->name)
        : fmt::format("USAGE:\n    {}", this->name);
    std::string flags_msg{"FLAGS:"}, options_msg{"OPTIONS:"}, subcommands_msg{"SUBCOMMANDS:"};
    bool has_flag = false;
    bool has_regular_option = false;
    bool has_subcommand = (this->subcommands.size() > 0);

    for (auto const &[subc_name, subc]: this->subcommands) {
        subcommands_msg += fmt::format("\n    {}\n", subc_name);
        if (subc->description.length() > 0) {
            subcommands_msg += fmt::format("        {}\n", subc->description);
        }
    }
    for (auto const &opt_def: this->option_definitions) {
        std::string opt_name{};
        if (opt_def->has_short_name() && opt_def->has_long_name()) {
            opt_name = fmt::format("-{}|--{}", opt_def->get_short_name().value(), opt_def->get_long_name().value());
        } else if (opt_def->has_short_name()) {
            opt_name = fmt::format("-{}", opt_def->get_short_name().value());
        } else if (opt_def->has_long_name()) {
            opt_name = fmt::format("--{}", opt_def->get_long_name().value());
        } else {
            // unreachable code, reaching this means there exists a partially declared option
            assert("unreachable code" && false);
        }
        if (opt_def->get_option_type() != EntryType::FLAG) {
            opt_name += fmt::format(" <{}>", opt_def->get_option_type_name());
        }
        std::string entry_help_msg{};
        if (opt_def->has_help_msg()) {
            entry_help_msg = fmt::format(R"(
    {}
        {}
)", opt_name, opt_def->get_help_msg().value());
        } else {
            entry_help_msg = fmt::format("\n    {}\n", opt_name);
        }
        switch (opt_def->get_option_type()) {
            case EntryType::UNDEFINED:
                throw std::runtime_error(fmt::format("uninitialized option type"));
            case EntryType::FLAG:
                has_flag = true;
                flags_msg += entry_help_msg;
                break;
            default:
                has_regular_option = true;
                options_msg += entry_help_msg;
        }
    }

    std::string combined_msg{""};
    if (has_flag) {
        usage_msg += " [FLAGS]";
        combined_msg += "\n" + flags_msg;
    }
    if (has_regular_option) {
        usage_msg += " [OPTIONS]";
        combined_msg += "\n" + options_msg;
    }
    if (has_subcommand) {
        usage_msg += " [SUBCOMMANDS]";
        combined_msg += "\n" + subcommands_msg;
    }
    std::string const display_name = this->supercommand.has_value()
        ? fmt::format("{}::{}", this->supercommand.value()->name, this->name)
        : this->name;
    std::string help_msg = fmt::format(
        "{}\n\n{}\n{}",
            this->description.length() > 0
                ? display_name + "\n" + this->description
                : display_name,
            usage_msg,
            combined_msg
    );
    return help_msg;
}

}
