{
  description = "cli^{2}b: A simple cpp CLI LIBrary for parsing command line subcommands/options/flags";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs {
      inherit system;
    };
    lib = nixpkgs.lib;
  in {
    packages = with pkgs; rec {
      default = cli2b;
      cli2b = stdenv.mkDerivation {
        pname = "cli2b";
        version = "0.3.0";
        src = ./.;
        nativeBuildInputs = [ meson ninja pkg-config ];
        buildInputs = [ fmt ];
        meta = {
          description = "cli^{2}b: A simple cpp CLI LIBrary for parsing command line subcommands/options/flags";
          homepage = "https://gitlab.com/highsunz/${pname}";
          license = lib.licenses.mit;
        };
      };
    };
  });
}
